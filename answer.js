'use strict'

const answerHandler = require('./handler')
/**
 * THIS ONE RUNS THE handler.js TO GIVE ANSWERS TO THE QUESTIONS
 */
console.info(`
 1. pagination                      
 --------------------------------------------------------------------------------------->

`)

const items = []
for (let i = 1; i <= 6; i ++) {
    items.push(`item ${i}`)
}

console.info('Imagine we have the following items\n', items, '\nAnd we want to display 5 items per page, and current page is 2, so we have: \n  answerHandler.limitItems(items, 5, 2)')

console.log(answerHandler.limitItems(items, 5, 2))

console.info('Now let\'s try display 3 items per page and current page is 3 (out of range): answerHandler.limitItems(items, 3, 3)')

console.log(answerHandler.limitItems(items, 3, 3))

console.info('As you can see, it reverted back to the last page which is 2')

console.info(`
 2. Get Authorised Devices                      
 --------------------------------------------------------------------------------------->

`)

console.info('Note the following is ACTUALLY loading from the database [data/device-authorisation.db]')

let user = {
    userId: 'patientB',
    name: 'Patient B',
    type: 'Patient'
}

answerHandler.getDevicesByAccount(user, (e, d) => {
    console.info('\n\n- Lets find devices for Patient B, so our input object is:\n')    
    console.log(user)
    console.info('\n\nNow we run this command to get all devices for her: answerHandler.getDevicesByAccount(user)\n')
    console.log(d)
})


user = {
    userId: 'doctorA',
    name: 'Doctor A',
    type: 'Doctor'
}

answerHandler.getDevicesByAccount(user, (e, d) => {
    console.info('\n\n- Lets find devices for Doctor A, so our input object is:\n')
    console.log(user)    
    console.info('\nNow we run this command to get all devices for her: answerHandler.getDevicesByAccount(user)')
    console.log(d)
})

