# Answer Sheet

## Prerequisites
Run `npm install` to install the packages

DB is already filled up however if you feel like running the seeding anyways (for instance, if you want to add more data - just edit the json files under `./data/`), run the following:
`npm run seeding`

## Answers
To see all the answers in demonstration, simply run
`npm run answer`

An example output is below, and for details of the answer, see below [*Files List*](#files-list):

![answer sheet](https://gitlab.com/coreorm/interview-problems/-/raw/master/screenshots/t2.png)

## Files list:
- `answer.js` is the file for demonstrating the answers;
- `handler.js` is the single entry point if you want to use this for lambda, just expand on the functions accordingly
- `./service/` contains all the supporting functions in which:
- `authorisedDevices.js` contains 2 functions, one for getting a single account by ID (`getAccountById`), the other for retrieving authorised devices from the db (`getDevicesByAccount`)
- `dao/readOnly.js` contains database access methods (read only)
- `./tests` contains all the tests (100% coverage for custom codes)
- `./data/seeding.js` contains seeding script to fill up the database - note that database is prefilled.
- `./data/device-authorisation.db` is the sqlite3 db for the second question
- edit `./data/accounts.json` and `./data/devices.json` if you want to test more data, remember to run `npm run seeding` afterwards to refill the database


## Testing
Run `npm install -g mocha` to setup mocha for testing
Run `npm test` to see all test results

#### Sample Test result (with Debug info)
![Test Result](https://gitlab.com/coreorm/interview-problems/-/raw/master/screenshots/t1.png)


### List of Devices from Database

### SQLite structure
I've created a simple sqlite db here `/data/device-authorisation.db`, schemas as follows:

```
CREATE TABLE accounts (
    accountId      STRING  PRIMARY KEY,
    name    STRING,
    clinic  STRING,
    type    STRING,
    consent BOOLEAN
);

CREATE TABLE devices (
    deviceId STRING PRIMARY KEY,
    name     STRING,
    owner    STRING REFERENCES accounts (accountId) 
);
```

Data injection is done via running the following script (it clears then reinserts)
`npm run seeding`
