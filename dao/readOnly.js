'use strict'

/**
 * *READ ONLY*
 * sqlite data access object
 * since it's ASYNC we open and close in each function
 * to avoid issues
 */
const sqlite3 = require('sqlite3').verbose()

/**
 * fetch one row
 */
const fetchOne = (sql, bind = [], cb) => {
    const db = new sqlite3.Database('./data/device-authorisation.db', sqlite3.OPEN_READONLY)
    let stmt = db.prepare(sql)
    stmt.run(bind)
    stmt.all((e, d) => {
        stmt.finalize()
        db.close()
        cb(e, d[0])
    })
}

/**
 * fetch all rows
 */
const fetchAll = (sql, bind = [], cb) => {
    const db = new sqlite3.Database('./data/device-authorisation.db', sqlite3.OPEN_READONLY)
    let stmt = db.prepare(sql)
    stmt.run(bind)
    stmt.all((e, d) => {
        cb(e, d)
        stmt.finalize()
        db.close()
    })
}


module.exports = {
    fetchOne,
    fetchAll
}