'use strict'

/** 
 * seeding script for second question
 * clears and reinsert each time you run
 * 
 * source is from ./accounts.json & devices.json
 */


// console.log(accounts)
const sqlite3 = require('sqlite3').verbose()
const db = new sqlite3.Database('./data/device-authorisation.db')

let cnt = 0;

// insert into accounts
// get accounts
let accounts = require('./accounts.json')
let sql = db.prepare(`
REPLACE INTO 
    accounts(accountId, name, clinic, type, consent) 
VALUES 
    (?, ?, ?, ?, ?)`)

accounts.forEach(account => {
    cnt ++
    sql.run([account.id, account.name, account.clinic, account.type, account.consent])
})
sql.finalize()
console.log(` - ${cnt} Rows inserted into accounts`);

// insert into devices
cnt = 0
let devices = require('./devices.json')
sql = db.prepare(`
REPLACE INTO 
    devices(deviceId, name, owner) 
VALUES 
    (?, ?, ?)`)

devices.forEach(device => {
    cnt ++
    sql.run([device.deviceId, device.name, device.owner])
})
sql.finalize()
console.log(` - ${cnt} Rows inserted into devices`);

db.close()