'use strict'

require('./header')


const limitItems = require('./service/limitItems')
const authService = require('./service/deviceAuthorisation')
const getAccountById = authService.getAccountById
const getDevicesByAccount = authService.getDevicesByAccount

/**
 * interview answer handlers
 */
module.exports = {
    limitItems,
    getDevicesByAccount,
    getAccountById
}