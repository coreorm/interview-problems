// debug level - default is fatal only
const loggerSetting = {level: 'fatal'}

// dev will log from info 
if (process.env.NODE_ENV === 'DEV') {    
    loggerSetting.level = 'info'
}

global.logger = require('tracer').colorConsole(loggerSetting)