'use strict'

const { darkolivegreen } = require('color-name')
const dao = require('../dao/readOnly')

/**
 * retrieve one account by ID
 * @param String id 
 * @param Callback cb 
 */
const getAccountById = (id, cb) => {
    logger.info(id)
    dao.fetchOne('SELECT * FROM accounts where accountId = ?', [id], cb)
}

/**
 * retrieve only valid devices for the current account
 * @param Object user 
 * @param Callback cb 
 */
const getDevicesByAccount = (user, cb) => {
    let sql = ''
    let bind = [user.userId]
    switch (user.type) {
        // for patients we only need to find from devices table
        case 'Patient':
            sql = 'SELECT * FROM devices WHERE owner = ?'
            break
        // for doctors we have to find all patients under them then grab their consent also
        // I'm lazy so I'm gonna use sql instead of js logic here (also saves a lot of connection time cos we use only 1 query)
        case 'Doctor':
            sql += `
            SELECT 
                d.* 
            FROM 
                devices d
            INNER JOIN
                accounts a ON a.accountId = d.owner
            WHERE 
                a.clinic = 
                (
                    SELECT 
                        clinic
                    FROM 
                        accounts 
                    WHERE
                        accountId = ? 
                )
            AND
                a.consent = 1
                `
            break
    }

    // query db to get all devices out
    dao.fetchAll(sql, bind, cb)
}

module.exports = {
    getAccountById,
    getDevicesByAccount
}