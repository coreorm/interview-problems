'use strict'

/**
 * pagination for items
 * @param Array items 
 * @param Int pageSize 
 * @param Int pageNumber 
 */
module.exports = (items, pageSize, pageNumber = 1) => {
    // items must be array
    let limitedItems = []

    if (typeof items != 'object' || !(items instanceof Array)) {
        throw new Error('Invalid items type - must be array')
    }

    if (typeof pageNumber != 'number' || typeof pageSize != 'number') {
        throw new Error('Invalid pageNumber or pageSize type - must be integer')
    }

    // cast to proper type
    pageNumber = parseInt(pageNumber)
    pageSize = parseInt(pageSize)

    const totalItems = items.length

    // total pages
    let pages = Math.ceil(totalItems/pageSize)

    if (pageNumber <1) {
        pageNumber = 1
    }
    
    if (pageNumber > pages) {
        pageNumber = pages
    }

    let startIndex = (pageNumber - 1) * pageSize
    let endIndex = Math.min(startIndex + 2, totalItems - 1)
    
    for (let i = startIndex; i <= endIndex; i ++) {
        limitedItems.push(items[i])
    }

    logger.info(startIndex, endIndex, limitedItems)

    return limitedItems
}