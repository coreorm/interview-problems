'use strict'

require('../header')
const expect = require('chai').expect
const service = require('../service/deviceAuthorisation')

/*
USER STORY
- Device database contains set of available devices in the organisation.

- A patient is a person account record having association with a clinic, devices, etc.

- Patient and doctors are associated with atleast one clinic. (can be multiple clinics)

- Each device has only 1 patient association and has a mandatory field named as owner.

- A user is authorised to view only related device associated with themself or associated with devices in their clinic.

- In addition, patient has given consent only to specific clinics to view their data. So make sure return data if clinic has consent from patient. If not, show message "You are not authorised to view data".

- if a patient logins, then function should only return the associated devices.

- if a doctor logins, then function should only return all the devices of patients associated in their clinic.

- The function should take in the current user and a set of devices. The authorisation function should filter the equipment argument based on the authorisation rules, returning only the devices the user is authorised to view.

- User has a user type attribute which defines the logged in userType.
*/
describe('Authorised Device Retrieval Test', function () {
    this.timeout(5000)

    it('Retrieve one account By Id', (done) => {
        service.getAccountById('doctorA', (e, d) => {
            console.log(e, d)
            expect(d.name).to.be.equal('Doctor A')
            done(e)
        })
    })

    it('Get devices for a patient', (done) => {
        let user = {
            "userId": "patientA",
            "type": "Patient",
            "name": "Patient A"
        }

        service.getDevicesByAccount(user, (e, d) => {
            console.log(e, d)
            expect(d).to.be.instanceOf(Array)
            expect(d[0].owner).to.equal(user.userId)
            done(e)
        })
    })

    it('Get devices for a doctor', (done) => {
        let user = {
            "userId": "doctorA",
            "type": "Doctor",
            "name": "Doctor A"
        }

        service.getDevicesByAccount(user, (e, d) => {
            console.log(e, d)
            expect(d).to.be.instanceOf(Array)
            expect(d[0].owner).to.include('patient')
            done(e)
        })
    })
})