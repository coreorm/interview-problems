'use strict'

require('../header')
const expect = require('chai').expect
const dao = require('../dao/readOnly')

// async tests, set timeout to 5 seconds
describe('Test Readonly DAO to sqlite', function () {
    this.timeout(5000)
    it('Single row of accounts - name must contain " B"', (done) => {
        try {
            dao.fetchOne(`SELECT * FROM accounts WHERE name LIKE ?`, '% B%', (e, d) => {
                logger.trace(e, d)
                expect(d).to.be.instanceOf(Object)
                expect(d.name).to.include(' B')
                done(e)
            })
        } catch (e) {
            done(e)
        }
    })

    it('Multiple rows of devices', (done) => {
        try {
            dao.fetchAll(`SELECT * FROM devices`, [], (e, d) => {
                logger.trace(e, d)
                expect(d).to.be.instanceOf(Array)
                done(e)
            })
        } catch (e) {
            done(e)
        }
    })
})