'use strict'

require('../header')
const expect = require('chai').expect
const limitItems = require('../service/limitItems')

describe('Limit items in an array', () => {
    it('Test for fail - items should be array only', () => {
        try {
            let items = limitItems({ foo: null }, 1, 1)
        } catch (e) {
            expect(e).to.be.instanceOf(Error)
            logger.info('>> Expected error: ' + e.toString())
        }
    })

    let items = [1, 2, {foo: 3}, 4, '5', 6, 7, 8, 9, 10]

    it('Test for success - get 3 items out of 2nd page of a 10 items array', () => {        
        let limitedItems = limitItems(items, 3, 2)
        expect(limitedItems.length).to.equal(3)
        expect(limitedItems[1]).to.equal(items[4])
    })

    it('Test for success - get less than 3 items out of range of a 10 items array', () => {
        // this should fall back to the last page
        let limitedItems = limitItems(items, 3, 1000)
        expect(limitedItems.length).to.be.lessThan(3)
        expect(limitedItems[0]).to.equal(items[9])
    })
})